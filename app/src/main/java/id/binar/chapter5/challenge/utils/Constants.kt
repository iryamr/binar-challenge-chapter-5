package id.binar.chapter5.challenge.utils

import id.binar.chapter5.challenge.BuildConfig

object Constants {

    const val BASE_URL = BuildConfig.BASE_URL
    const val IMG_URL = BuildConfig.IMG_URL
    const val API_KEY = BuildConfig.API_KEY

    const val MIN_PASSWORD_LENGTH = 6
}