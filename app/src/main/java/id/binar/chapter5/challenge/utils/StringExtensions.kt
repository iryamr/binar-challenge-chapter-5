package id.binar.chapter5.challenge.utils

import android.util.Patterns
import java.text.SimpleDateFormat
import java.util.*

fun String.isValidated(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.toDate(): String? {
    val inputPattern = "yyyy-MM-dd"
    val outputPattern = "MMM dd, yyyy"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())

    val inputDate = inputFormat.parse(this)

    return inputDate?.let {
        outputFormat.format(it)
    }
}