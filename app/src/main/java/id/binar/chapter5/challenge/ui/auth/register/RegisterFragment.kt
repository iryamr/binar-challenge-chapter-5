package id.binar.chapter5.challenge.ui.auth.register

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.databinding.FragmentRegisterBinding
import id.binar.chapter5.challenge.ui.home.HomeActivity
import id.binar.chapter5.challenge.utils.Constants.MIN_PASSWORD_LENGTH
import id.binar.chapter5.challenge.utils.ViewUtils.showToast
import id.binar.chapter5.challenge.utils.isValidated

@AndroidEntryPoint
class RegisterFragment : Fragment(R.layout.fragment_register) {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: RegisterViewModel by viewModels()

    private val observerRegister: Observer<Long> = Observer { result ->
        when (result) {
            0L -> showToast(requireContext(), "Something is wrong")
            else -> moveToHomeActivity()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentRegisterBinding.bind(view)

        backToLogin()
        register()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun register() {
        binding.btnRegister.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()
            val passwordConfirm = binding.etPasswordConfirm.text.toString()

            if (validateData(username, email, password, passwordConfirm)) {
                viewModel.checkEmail(email).observe(viewLifecycleOwner) { isExist ->
                    if (isExist) {
                        binding.etlEmail.error = "Email sudah ada"
                        binding.etlEmail.requestFocus()
                    } else {
                        viewModel.register(username, email, password)
                            .observe(viewLifecycleOwner, observerRegister)
                    }
                }
            }
        }
    }

    private fun validateData(
        username: String,
        email: String,
        password: String,
        passwordConfirm: String
    ): Boolean {
        return when {
            username.isEmpty() -> {
                binding.etlUsername.error = "Username tidak boleh kosong"
                binding.etlUsername.requestFocus()
                false
            }
            email.isEmpty() -> {
                binding.etlEmail.error = "Email tidak boleh kosong"
                binding.etlEmail.requestFocus()
                false
            }
            !email.isValidated() -> {
                binding.etlEmail.error = "Email tidak valid"
                binding.etlEmail.requestFocus()
                false
            }
            password.isEmpty() -> {
                binding.etlPassword.error = "Password tidak boleh kosong"
                binding.etlPassword.requestFocus()
                false
            }
            password.length < MIN_PASSWORD_LENGTH -> {
                binding.etlPassword.error =
                    "Password harus lebih dari $MIN_PASSWORD_LENGTH karakter"
                binding.etlPassword.requestFocus()
                false
            }
            passwordConfirm.isEmpty() -> {
                binding.etlPasswordConfirm.error = "Konfirmasi password tidak boleh kosong"
                binding.etlPasswordConfirm.requestFocus()
                false
            }
            passwordConfirm != password -> {
                binding.etlPasswordConfirm.error = "Password tidak sesuai"
                binding.etlPasswordConfirm.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun moveToHomeActivity() {
        Intent(requireContext(), HomeActivity::class.java).also { intent ->
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun backToLogin() {
        binding.tvLogin.setOnClickListener { it.findNavController().popBackStack() }
    }
}