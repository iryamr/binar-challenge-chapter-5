package id.binar.chapter5.challenge.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.source.local.UserPreference
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val prefs: UserPreference
) : ViewModel() {

    fun getUser(): LiveData<UserEntity> = prefs.getUser().asLiveData()
}