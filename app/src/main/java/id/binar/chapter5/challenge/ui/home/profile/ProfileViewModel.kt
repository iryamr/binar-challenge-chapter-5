package id.binar.chapter5.challenge.ui.home.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.repository.AuthRepository
import id.binar.chapter5.challenge.data.source.local.UserPreference
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val prefs: UserPreference
) : ViewModel() {

//    private val update = MutableLiveData<Int>()
private var update = 0

//    fun getData(email: String): LiveData<UserEntity> {
//        viewModelScope.launch {
//            user.value = repository.getUser(email).value
//        }
//
//        return user
//    }

    fun updateProfile(
        user: UserEntity,
    ): Int {
//        val user = UserEntity(
//            username = username,
//            name = name,
//            dateOfBirth = date,
//            address = address,
//        )

        viewModelScope.launch {
            update = repository.updateProfile(user)
        }

        return update
    }

    fun logout() {
        viewModelScope.launch {
            prefs.logout()
        }
    }
}