package id.binar.chapter5.challenge.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.data.source.local.room.dao.UserDao
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val userDao: UserDao
) {

    fun getUser(email: String): LiveData<UserEntity> = liveData {
        val result: LiveData<UserEntity> = userDao.getUser(email)
        emitSource(result)
    }

    fun login(email: String, password: String): LiveData<Result<UserEntity>> = liveData {
        emit(Result.Loading)
        try {
            val result: LiveData<Result<UserEntity>> =
                userDao.login(email, password).map { Result.Success(it) }
            emitSource(result)
        } catch (e: Exception) {
            emit(Result.Error(e.message.toString()))
        }
    }

    suspend fun register(user: UserEntity): Long =
        userDao.register(user)

    suspend fun updateProfile(user: UserEntity): Int =
        userDao.updateProfile(user)

    suspend fun checkEmail(email: String): Boolean =
        userDao.checkEmail(email)

    suspend fun checkCredentials(email: String?, password: String?): Boolean =
        userDao.checkCredentials(email, password)
}