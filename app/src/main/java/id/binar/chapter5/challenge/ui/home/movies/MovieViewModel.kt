package id.binar.chapter5.challenge.ui.home.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.repository.AuthRepository
import id.binar.chapter5.challenge.data.repository.MovieRepository
import id.binar.chapter5.challenge.data.source.local.UserPreference
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val movieRepository: MovieRepository,
    private val authRepository: AuthRepository,
    private val prefs: UserPreference
) : ViewModel() {

    fun getMovies() = movieRepository.getMovies()

    fun getUser(email: String) = authRepository.getUser(email)

    fun getEmail() = prefs.getUser().asLiveData()
}