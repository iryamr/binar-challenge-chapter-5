package id.binar.chapter5.challenge.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.databinding.FragmentLoginBinding
import id.binar.chapter5.challenge.ui.home.HomeActivity
import id.binar.chapter5.challenge.utils.Constants.MIN_PASSWORD_LENGTH
import id.binar.chapter5.challenge.utils.ViewUtils.showToast
import id.binar.chapter5.challenge.utils.isValidated

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login) {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LoginViewModel by viewModels()

    private val observer: Observer<Result<UserEntity>> = Observer { result ->
        when (result) {
            is Result.Loading -> {
                setLoadState(true)
            }
            is Result.Success -> {
                setLoadState(false)
                checkUser(result.data)
            }
            is Result.Error -> {
                setLoadState(false)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentLoginBinding.bind(view)

        login()
        moveToRegister()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun login() {
        binding.btnLogin.setOnClickListener {
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            if (validateData(email, password)) {
                viewModel.login(email, password).observe(viewLifecycleOwner, observer)
            }
        }
    }

    private fun validateData(email: String, password: String): Boolean {
        return when {
            email.isEmpty() -> {
                binding.etlEmail.error = "Email tidak boleh kosong"
                binding.etlEmail.requestFocus()
                false
            }
            !email.isValidated() -> {
                binding.etlEmail.error = "Email tidak valid"
                binding.etlEmail.requestFocus()
                false
            }
            password.isEmpty() -> {
                binding.etlPassword.error = "Password tidak boleh kosong"
                binding.etlPassword.requestFocus()
                false
            }
            password.length < MIN_PASSWORD_LENGTH -> {
                binding.etlPassword.error =
                    "Password harus lebih dari $MIN_PASSWORD_LENGTH karakter"
                binding.etlPassword.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun checkUser(user: UserEntity?) {
        if (user != null) {
            if (viewModel.checkCredentials(user.email, user.password)) {
                moveToHome()
            } else {
                binding.etlPassword.error = "Password salah"
            }
        } else {
            showToast(requireContext(), "User tidak ditemukan")
        }
    }

    private fun moveToRegister() {
        binding.tvRegister.setOnClickListener {
            it.findNavController()
                .navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
    }

    private fun moveToHome() {
        Intent(requireContext(), HomeActivity::class.java).also { intent ->
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun setLoadState(isLoading: Boolean) {
        binding.progressBar.isVisible = isLoading
    }
}