package id.binar.chapter5.challenge.ui.auth.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.repository.AuthRepository
import id.binar.chapter5.challenge.data.source.local.UserPreference
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val prefs: UserPreference
) : ViewModel() {

    private val register = MutableLiveData<Long>()
    private var isExist = MutableLiveData<Boolean>()

    fun register(username: String, email: String, password: String): LiveData<Long> {
        val user = UserEntity(
            username = username,
            email = email,
            password = password,
        )

        viewModelScope.launch {
            register.value = repository.register(user)
            setLogin(email)
        }

        return register
    }

    private fun setLogin(email: String) {
        viewModelScope.launch {
            prefs.login(email)
        }
    }

    fun checkEmail(email: String): LiveData<Boolean> {
        viewModelScope.launch {
            isExist.value = repository.checkEmail(email)
        }

        return isExist
    }
}