package id.binar.chapter5.challenge.data.source.local

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UserPreference @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    fun getUser(): Flow<UserEntity> {
        return dataStore.data.map { preferences ->
            UserEntity(
                preferences[ID_KEY] ?: 0,
                preferences[USERNAME_KEY] ?: "",
                preferences[NAME_KEY] ?: "",
                preferences[EMAIL_KEY] ?: "",
                preferences[PASSWORD_KEY] ?: "",
                preferences[ADDRESS_KEY] ?: "",
                preferences[DATE_KEY] ?: "",
                preferences[STATE_KEY] ?: false
            )
        }
    }

    suspend fun checkLogin(): Boolean? {
        val preference = dataStore.data.first()
        return preference[STATE_KEY]
    }

    suspend fun login(email: String) {
        dataStore.edit { preferences ->
            preferences[EMAIL_KEY] = email
            preferences[STATE_KEY] = true
        }
    }

    suspend fun logout() {
        dataStore.edit { preferences ->
            preferences[STATE_KEY] = false
        }
    }

    companion object {
        private val ID_KEY = intPreferencesKey("id")
        private val USERNAME_KEY = stringPreferencesKey("username")
        private val NAME_KEY = stringPreferencesKey("name")
        private val EMAIL_KEY = stringPreferencesKey("email")
        private val PASSWORD_KEY = stringPreferencesKey("password")
        private val ADDRESS_KEY = stringPreferencesKey("address")
        private val DATE_KEY = stringPreferencesKey("date")
        private val STATE_KEY = booleanPreferencesKey("state")
    }
}