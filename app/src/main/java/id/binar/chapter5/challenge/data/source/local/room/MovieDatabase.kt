package id.binar.chapter5.challenge.data.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import id.binar.chapter5.challenge.data.source.local.entity.MovieEntity
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.data.source.local.room.dao.MovieDao
import id.binar.chapter5.challenge.data.source.local.room.dao.UserDao

@Database(entities = [UserEntity::class, MovieEntity::class], version = 1, exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {

    abstract val movieDao: MovieDao
    abstract val userDao: UserDao
}