package id.binar.chapter5.challenge.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.databinding.ActivityAuthBinding
import id.binar.chapter5.challenge.ui.home.HomeActivity

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding

    private val viewModel: AuthViewModel by viewModels()

    private val observer: Observer<UserEntity> = Observer { user ->
        if (user.isLogin) {
            Intent(this, HomeActivity::class.java).also { intent ->
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkAuth()
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun checkAuth() {
        viewModel.getUser().observe(this, observer)
    }
}