package id.binar.chapter5.challenge.ui.home.detail

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.source.local.entity.MovieEntity
import id.binar.chapter5.challenge.databinding.FragmentDetailMovieBinding
import id.binar.chapter5.challenge.utils.loadImage
import id.binar.chapter5.challenge.utils.toDate

@AndroidEntryPoint
class DetailMovieFragment : Fragment(R.layout.fragment_detail_movie) {

    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    private val viewModel: DetailMovieViewModel by viewModels()
    private val observer: Observer<Result<MovieEntity>> = Observer { result ->
        when (result) {
            is Result.Loading -> {
                setLoadState(true)
            }
            is Result.Success -> {
                setLoadState(false)
                showData(result.data)
            }
            is Result.Error -> {
                setLoadState(false)
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentDetailMovieBinding.bind(view)

        onBackPressed()
        setDetailMovie()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setDetailMovie() {
        val movieId = DetailMovieFragmentArgs.fromBundle(arguments as Bundle).movieId
        viewModel.getDetailMovie(movieId).observe(viewLifecycleOwner, observer)
    }

    private fun showData(movie: MovieEntity?) {
        binding.apply {
            ivBackdrop.loadImage(movie?.backdropPath)
            ivPoster.loadImage(movie?.posterPath)
            tvRate.text = movie?.voteAverage.toString()
            tvReleaseDate.text = if (movie?.releaseDate != "") movie?.releaseDate?.toDate() else "-"
            tvTitle.text = movie?.title
            tvPopularity.text = getString(R.string.popularity, movie?.popularity.toString())
            tvVoteCount.text = getString(R.string.vote_count, movie?.voteCount.toString())
            tvOverview.text = movie?.overview
        }
    }

    private fun onBackPressed() {
        binding.btnBack.setOnClickListener { it.findNavController().popBackStack() }
    }

    private fun setLoadState(isLoading: Boolean) {
        binding.progressBar.isVisible = isLoading
    }
}