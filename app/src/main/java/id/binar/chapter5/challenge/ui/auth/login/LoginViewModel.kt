package id.binar.chapter5.challenge.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.repository.AuthRepository
import id.binar.chapter5.challenge.data.source.local.UserPreference
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val prefs: UserPreference
) : ViewModel() {

    private val result = MutableLiveData<Result<UserEntity>>()
    private var isValid = false

    fun login(email: String, password: String): LiveData<Result<UserEntity>> {
        viewModelScope.launch {
            result.value = repository.login(email, password).value
            setLogin(email)
        }

        return result
    }

    private fun setLogin(email: String) {
        viewModelScope.launch {
            prefs.login(email)
        }
    }

    fun checkCredentials(email: String?, password: String?): Boolean {
        viewModelScope.launch {
            isValid = repository.checkCredentials(email, password)
        }

        return isValid
    }
}