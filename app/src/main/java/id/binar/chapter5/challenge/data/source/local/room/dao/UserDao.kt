package id.binar.chapter5.challenge.data.source.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity

@Dao
interface UserDao {

    @Query("SELECT * FROM users WHERE email = :email LIMIT 1")
    fun getUser(email: String): LiveData<UserEntity>

    @Query("SELECT * FROM users WHERE email = :email AND password = :password")
    fun login(email: String, password: String): LiveData<UserEntity>

    @Insert
    suspend fun register(user: UserEntity): Long

    @Update
    suspend fun updateProfile(user: UserEntity): Int

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email)")
    suspend fun checkEmail(email: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email AND password = :password LIMIT 1)")
    suspend fun checkCredentials(email: String?, password: String?): Boolean
}