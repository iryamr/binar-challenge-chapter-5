package id.binar.chapter5.challenge.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.source.local.entity.MovieEntity
import id.binar.chapter5.challenge.data.source.local.room.dao.MovieDao
import id.binar.chapter5.challenge.data.source.remote.api.ApiService
import id.binar.chapter5.challenge.utils.Constants.API_KEY
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val apiService: ApiService,
    private val movieDao: MovieDao
) {

    fun getMovies(): LiveData<Result<List<MovieEntity>>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.getMovies(API_KEY)
            val movies = response.results
            val movieList = movies.map { movie ->
                movie.toMovieEntity()
            }
            movieDao.deleteMovies()
            movieDao.insertMovies(movieList)
        } catch (e: Exception) {
            emit(Result.Error(e.message.toString()))
        }
        val localData: LiveData<Result<List<MovieEntity>>> =
            movieDao.getMovies().map { Result.Success(it) }
        emitSource(localData)
    }

    fun getDetailMovie(movieId: Int): LiveData<Result<MovieEntity>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.getDetailMovie(API_KEY, movieId)
            val movie = response.toMovieEntity()
            movieDao.deleteMovies()
            movieDao.insertMovie(movie)
        } catch (e: Exception) {
            emit(Result.Error(e.message.toString()))
        }
        val localData: LiveData<Result<MovieEntity>> =
            movieDao.getMovie(movieId).map { Result.Success(it) }
        emitSource(localData)
    }
}