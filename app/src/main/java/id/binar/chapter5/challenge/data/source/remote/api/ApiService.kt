package id.binar.chapter5.challenge.data.source.remote.api

import id.binar.chapter5.challenge.data.source.remote.response.Movie
import id.binar.chapter5.challenge.data.source.remote.response.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/now_playing")
    suspend fun getMovies(
        @Query("api_key") apiKey: String
    ): MovieResponse

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Query("api_key") apiKey: String,
        @Path("movie_id") movieId: Int
    ): Movie
}