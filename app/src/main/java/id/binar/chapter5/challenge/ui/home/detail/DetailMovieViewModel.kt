package id.binar.chapter5.challenge.ui.home.detail

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter5.challenge.data.repository.MovieRepository
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    fun getDetailMovie(movieId: Int) = repository.getDetailMovie(movieId)
}