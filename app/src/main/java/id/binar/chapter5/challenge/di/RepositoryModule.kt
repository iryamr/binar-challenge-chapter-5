package id.binar.chapter5.challenge.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.binar.chapter5.challenge.data.repository.AuthRepository
import id.binar.chapter5.challenge.data.repository.MovieRepository
import id.binar.chapter5.challenge.data.source.local.room.dao.MovieDao
import id.binar.chapter5.challenge.data.source.local.room.dao.UserDao
import id.binar.chapter5.challenge.data.source.remote.api.ApiService
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(apiService: ApiService, movieDao: MovieDao) =
        MovieRepository(apiService, movieDao)

    @Provides
    @Singleton
    fun provideAuthRepository(userDao: UserDao) =
        AuthRepository(userDao)
}