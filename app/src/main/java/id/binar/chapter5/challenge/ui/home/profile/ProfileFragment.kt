package id.binar.chapter5.challenge.ui.home.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.databinding.FragmentProfileBinding
import id.binar.chapter5.challenge.ui.auth.AuthActivity
import id.binar.chapter5.challenge.utils.ViewUtils.showToast

@AndroidEntryPoint
class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private var user: UserEntity? = null

    private val viewModel: ProfileViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentProfileBinding.bind(view)

        setData()
        updateProfile()
        logout()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setData() {
        user = ProfileFragmentArgs.fromBundle(arguments as Bundle).user

        user?.let {
            binding.apply {
                etUsername.setText(it.username)
                etName.setText(it.name)
                etDate.setText(it.dateOfBirth)
                etAddress.setText(it.address)
            }
        }
    }

    private fun updateProfile() {
        binding.btnUpdate.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val name = binding.etName.text.toString()
            val date = binding.etDate.text.toString()
            val address = binding.etAddress.text.toString()

            if (validateData(username, name, date, address)) {
                user?.let {
                    it.username = username
                    it.name = name
                    it.dateOfBirth = date
                    it.address = address

                    when (viewModel.updateProfile(it)) {
                        0 -> {
                            showToast(requireContext(), "Profil berhasil diperbarui")
                            view?.findNavController()?.popBackStack()
                        }
                        else -> {
                            showToast(requireContext(), "Profil gagal diperbarui")
                        }
                    }
                }

            }
        }
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            viewModel.logout()
            moveToLogin()
        }
    }

    private fun validateData(
        username: String,
        name: String,
        date: String,
        address: String
    ): Boolean {
        return when {
            username.isEmpty() -> {
                binding.etlUsername.error = "Username tidak boleh kosong"
                binding.etlUsername.requestFocus()
                false
            }
            name.isEmpty() -> {
                binding.etlName.error = "Email tidak boleh kosong"
                binding.etlName.requestFocus()
                false
            }
            date.isEmpty() -> {
                binding.etlDate.error = "Password tidak boleh kosong"
                binding.etlDate.requestFocus()
                false
            }
            address.isEmpty() -> {
                binding.etlAddress.error = "Konfirmasi password tidak boleh kosong"
                binding.etlAddress.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun moveToLogin() {
        Intent(requireContext(), AuthActivity::class.java).also { intent ->
            startActivity(intent)
            requireActivity().finish()
        }
    }
}