package id.binar.chapter5.challenge.utils

import androidx.constraintlayout.utils.widget.ImageFilterView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.imageview.ShapeableImageView
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.utils.Constants.IMG_URL

fun ShapeableImageView.loadImage(image: String?) {
    Glide.with(this.context)
        .load(IMG_URL + image)
        .apply(
            RequestOptions
                .placeholderOf(R.drawable.ic_error)
                .error(R.drawable.ic_error)
        )
        .into(this)
}

fun ImageFilterView.loadImage(image: String?) {
    Glide.with(this.context)
        .load(IMG_URL + image)
        .apply(
            RequestOptions
                .placeholderOf(R.drawable.ic_error)
                .error(R.drawable.ic_error)
        )
        .into(this)
}