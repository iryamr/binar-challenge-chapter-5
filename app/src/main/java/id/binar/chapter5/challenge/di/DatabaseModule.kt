package id.binar.chapter5.challenge.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.binar.chapter5.challenge.data.source.local.room.MovieDatabase
import id.binar.chapter5.challenge.data.source.local.room.dao.MovieDao
import id.binar.chapter5.challenge.data.source.local.room.dao.UserDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): MovieDatabase =
        Room.databaseBuilder(context, MovieDatabase::class.java, "Movie.db").build()

    @Provides
    fun provideMovieDao(database: MovieDatabase): MovieDao =
        database.movieDao

    @Provides
    fun provideUserDao(database: MovieDatabase): UserDao =
        database.userDao
}