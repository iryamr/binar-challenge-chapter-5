package id.binar.chapter5.challenge.ui.home.movies

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter5.challenge.R
import id.binar.chapter5.challenge.data.Result
import id.binar.chapter5.challenge.data.source.local.entity.MovieEntity
import id.binar.chapter5.challenge.data.source.local.entity.UserEntity
import id.binar.chapter5.challenge.databinding.FragmentMovieBinding
import id.binar.chapter5.challenge.ui.adpater.MovieAdapter

@AndroidEntryPoint
class MovieFragment : Fragment(R.layout.fragment_movie) {

    private var _binding: FragmentMovieBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels()

    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter(::onMovieClicked)
    }

    private val observer: Observer<Result<List<MovieEntity>>> = Observer { result ->
        when (result) {
            is Result.Loading -> {
                setLoadState(true)
            }
            is Result.Success -> {
                setLoadState(false)
                val movies = result.data
                movieAdapter.submitList(movies)
            }
            is Result.Error -> {
                setLoadState(false)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMovieBinding.bind(view)

        setMovies()
        setUserData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setMovies() {
        viewModel.getMovies().observe(viewLifecycleOwner, observer)
        binding.rvMovies.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = movieAdapter
        }
    }

    private fun setUserData() {
        viewModel.getEmail().observe(viewLifecycleOwner) { user ->
            viewModel.getUser(user.email as String).observe(viewLifecycleOwner) { result ->
                binding.tvGreeting.text = getString(R.string.greeting, result.username)
                moveToProfile(result)
            }
        }
    }

    private fun onMovieClicked(movie: MovieEntity) {
        view?.findNavController()
            ?.navigate(MovieFragmentDirections.actionMovieFragmentToDetailMovieFragment(movie.id))
    }

    private fun moveToProfile(user: UserEntity) {
        binding.btnProfile.setOnClickListener {
            val action = MovieFragmentDirections.actionMovieFragmentToProfileFragment(user)
            it.findNavController().navigate(action)
        }
    }

    private fun setLoadState(isLoading: Boolean) {
        binding.progressBar.isVisible = isLoading
        binding.rvMovies.isVisible = !isLoading
    }
}